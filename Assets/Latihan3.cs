﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Latihan3 : MonoBehaviour
{

   

    // Start is called before the first frame update
    void Start()
    {
        int c = Jumlah(60, 40);
        Debug.Log("Hasil Jumlah a + b = " + c);
        
        
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
    /// <summary>
    /// Fungsi penjumlahan 2 angka
    /// </summary>
    /// <param name="a">Masukkan Angka Integer</param>
    /// <param name="b">Masukkan Angka Integer</param>
    int Jumlah(int a, int b)
    {
        return a + b;
    }
}
