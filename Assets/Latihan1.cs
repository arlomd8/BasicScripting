﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Latihan1 : MonoBehaviour
{
    // Start is called before the first frame update
    
    void Awake()
    {
        Debug.Log("Awake OK!");
    }

    void Start()
    {
        Debug.Log("Hello Dicoding! I'm Arlo, the programmer and the developer, ANNDDDD OPEEEENNNN !!!");
    }

    // Update is called once per frame

    void FixedUpdate()
    {
        Debug.Log("Waktu Fixed Update " + Time.deltaTime);
    }

    void Update()
    {
        Debug.Log("Waktu Update " + Time.deltaTime);
    }


    void LateUpdate()
    {
        Debug.Log("Waktu Late Update " + Time.deltaTime);
    }
}
